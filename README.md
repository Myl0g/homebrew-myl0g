# homebrew-myl0g

Homebrew tap for my open-source projects.

## Usage

```bash
brew tap Myl0g/homebrew-myl0g https://gitlab.com/myl0g/homebrew-myl0g.git
brew install myl0g/homebrew-myl0g/"some formula in the Formula folder"
```

## Updating a Formula (Maintainers)

Change the file download URL, then run the following to get the new checksum:

```bash
brew fetch [file] --build-from-source
```
