class Bpg < Formula
  desc "Wraps around GPG to provide a better interface"
  homepage "https://gitlab.com/myl0g/bpg"
  url "https://gitlab.com/Myl0g/bpg/uploads/0a8258b25a3b93f70f3aeb1674561822/bpg-v2.2.1-macOS.zip"
  sha256 "ca6caa2f65f3c57f275dc1dae663cd0e7db926f996c0654c4da3cc448723a787"
  depends_on "gnupg"
  depends_on "pinentry-mac"

  def install
    mkdir "#{bin}"
    cp "bpg", "#{bin}"
  end

  def caveats; <<~EOS
    You must run the following commands to complete installation of pinentry-mac:

    echo "pinentry-program /usr/local/bin/pinentry-mac" >> ~/.gnupg/gpg-agent.conf
    killall gpg-agent
  EOS
  end

  test do
    system "bpg", "help"
  end
end
